def IsYearLeap(year):
    retorn=None # creo variable auxiliar para guardar si cumple o no el requisitio, año bisiesto ha de ser multiple de 4, pero si es multiple de 100 ha de serlo de 400
    year = year

    if year %4==0:
        retorn=True
        if year %100==0:
            #print("si 100")
            if year %400 == 0:
                #print("si 400")
                retorn= True
            else:
                #print("None, no 400")
                retorn= None
        return retorn

    else:
        return None


def DaysInMonth(year,month):
    year=year
    month=month

    if month == 2:
        if IsYearLeap(year):
            return 29
        else:
            return 28
    if month ==1 or 3 or 5 or 7 or 8 or 10 or 12:
        return 31

    if month == 4 or 6 or 9 or 11:
        return 30

    else:
        return None


def DayOfYear(year,month,day):
    year=year
    month=month
    day=day


    if month >12 or month <0:
        #print("MES INCORRECTE",day, month, year, sep="/")
        return None
    if day>=1 and day<=31:
        if DaysInMonth(year,month)< day:
            #print("EL MES TE MENYS DIES QUE EL DIA", day, month, year, sep="/")
            return None
        elif DaysInMonth(year,month)<= day and day >0:
            print("CORRECTE",day,month,year,sep="/")
            return True
    else:
        #print(" ALTRES CASOS ", day, month, year, sep="/")
        return None



print(DayOfYear(2000,15,31)) #year , month , days
print(DayOfYear(2000,5,23)) #year , month , days
print(DayOfYear(2007,8,88)) #year , month , days

print(DayOfYear(2000,10,31)) #year , month , days
print(DayOfYear(2000,2,29)) #year , month , days
print(DayOfYear(2003,2,29)) #year , month , days